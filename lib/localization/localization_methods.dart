import 'package:custom_widgets/localization/set_localization.dart';
import 'package:flutter/material.dart';

String tr(BuildContext context, String key) {
  if (key == null) return "tr key is null";
  return SetLocalization.of(context)!.getTranslateValue(key);
}
