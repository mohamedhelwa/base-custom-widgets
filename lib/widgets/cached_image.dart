import 'package:cached_network_image/cached_network_image.dart';
import 'package:custom_widgets/utils/widget_utils.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class CachedImage extends StatefulWidget {
  final String url;
  final BoxFit? fit;
  final double? height, width;
  final BorderRadius? borderRadius;
  final ColorFilter? colorFilter;
  final Alignment? alignment;
  final Widget? child;
  final Widget? placeHolder;
  final Color? borderColor;
  final Color? bgColor;
  final BoxShape? boxShape;
  final bool haveRadius;
  final bool? clearMemoryCache;

  CachedImage({
    required this.url,
    this.fit,
    this.width,
    this.height,
    this.placeHolder,
    this.borderRadius,
    this.colorFilter,
    this.alignment,
    this.child,
    this.boxShape,
    this.borderColor,
    this.bgColor,
    this.haveRadius = true,
    this.clearMemoryCache = false,
  });

  @override
  State<CachedImage> createState() => _CachedImageState();
}

class _CachedImageState extends State<CachedImage> {
  int _errorCount = 0;

  @override
  Widget build(BuildContext context) {
    if (widget.clearMemoryCache != null) {
      if (widget.clearMemoryCache!) {
        _checkMemory();
      }
    }

    return CachedNetworkImage(
      imageUrl: "${widget.url}",
      width: widget.width,
      height: widget.height,
      imageBuilder: (context, imageProvider) {
        return Container(
          width: widget.width,
          height: widget.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: imageProvider,
              fit: widget.fit ?? BoxFit.fill,
              colorFilter: widget.colorFilter,
            ),
            borderRadius: widget.haveRadius
                ? widget.borderRadius ?? BorderRadius.circular(0)
                : null,
            shape: widget.boxShape ?? BoxShape.rectangle,
            border: Border.all(
                color: widget.borderColor ?? Colors.transparent, width: 1),
          ),
          alignment: widget.alignment ?? Alignment.center,
          child: widget.child,
        );
      },
      placeholder: (context, url) {
        return Container(
          width: widget.width,
          height: widget.height,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            borderRadius: widget.haveRadius
                ? widget.borderRadius ?? BorderRadius.circular(0)
                : null,
            border: Border.all(
                color: widget.borderColor ?? Colors.transparent, width: 1),
            shape: widget.boxShape ?? BoxShape.rectangle,
            color: widget.bgColor ?? WidgetUtils.primaryColor.withOpacity(.5),
          ),
          child: SpinKitFadingCircle(
            color: WidgetUtils.primaryColor,
            size: 30.0,
          ),
        );
      },
      errorWidget: (context, url, error) {
        if (_errorCount < 2) {
          _errorCount++;
          if (mounted) {
            Future.delayed(const Duration(milliseconds: 100), () {
              setState(() {});
            });
          }
        }
        return Container(
          width: widget.width,
          height: widget.height,
          alignment: Alignment.center,
          decoration: BoxDecoration(
            color: widget.bgColor ?? WidgetUtils.primaryColor.withOpacity(.5),
            borderRadius: widget.haveRadius
                ? widget.borderRadius ?? BorderRadius.circular(0)
                : null,
            border: Border.all(
                color: widget.borderColor ?? Colors.transparent, width: 1),
            shape: widget.boxShape ?? BoxShape.rectangle,
          ),
          child: Stack(
            children: [
              widget.placeHolder ?? widget.child ?? Container(),
              widget.placeHolder ??
                  Center(child: Image.asset("assets/images/placeholder.png")),
            ],
          ),
        );
      },
    );
  }

  void _checkMemory() {
    ImageCache imageCache = PaintingBinding.instance.imageCache;
    if (imageCache.currentSizeBytes >= 55 << 20) {
      print('##### Clearing image cache #####');
      print("Cache size: ${imageCache.currentSizeBytes}");
      imageCache.clear();
      imageCache.clearLiveImages();
      print("Current Cache Size: ${imageCache.currentSizeBytes}");
    }
  }
}
